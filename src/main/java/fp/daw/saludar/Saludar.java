package fp.daw.saludar;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/saludar")
public class Saludar extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    
    
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		PrintWriter out = new PrintWriter(response.getOutputStream());
		String nombre= request.getParameter("nombre");
		
		try {
				out.println("<!DOCTYPE html>");
				out.println("<html>");
				out.println("<head>");
				out.println("<meta charset=\"utf-8\">");
				out.println("<title>Primera aplicacion jee</title>");
				out.println("</head>");
				out.println("<body>");
				out.println("<p>Hola "+nombre+"</p>");
				out.println("</body>");
				out.println("</html>");
		}finally{
			out.close();
		};
		
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		doGet(request, response);
	}

}
